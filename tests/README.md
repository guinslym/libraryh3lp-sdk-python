### Testing

    pip install -r dev-requirements.txt

### Requirement

* Add a **config** and **credentials** in this tests folder

test_config::

    [default]
    server = libraryh3lp.com
    timezone = UTC
    salt = "you should probably change this"

test_credentials::

    [default]
    username = <ADMIN_USER>
    password = <ADMIN_PASS>