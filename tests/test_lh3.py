import os 
import sys
import unittest 
import pytest
import datetime
from datetime import time
from lh3 import api as l
import warnings
warnings.filterwarnings('ignore')

import requests

current_directory = os.path.expanduser(os.path.dirname(os.path.abspath(__file__)))
config_test_file = current_directory+'/test_config'
credentials_test_file = current_directory+'/test_credentials'

def isNowInTimePeriod(startTime, endTime, nowTime):
    if startTime < endTime:
        return nowTime >= startTime and nowTime <= endTime
    else: #Over midnight
        return nowTime >= startTime or nowTime <= endTime

def is_time_between(begin_time, end_time, check_time=None):
    # If check time is not given, default to current UTC time
    check_time = check_time or datetime.utcnow().time()
    if begin_time < end_time:
        return check_time >= begin_time and check_time <= end_time
    else: # crosses midnight
        return check_time >= begin_time or check_time <= end_time

def check_if_there_is_a_file():
    is_file =  os.path.exists(os.path.expanduser(current_directory+'/secrets.json'))
    #import pdb; pdb.set_trace()
    return is_file

class TestLH3API(object):
        
    def test_raise_error_when_there_is_no_config_file(self):
        with pytest.raises(l.lh3er.FileDoesNotExist) as excinfo:  
            client = l.Client(
                filepath={'config': '~/.lh3/configs', 'credentials': '~/.lh3/credentials'}
                ) 
            assert "Please check if you have those files" in str(excinfo.value)

    def test_is_admin(self):
        client = l.Client() 
        assert client.is_admin() == True
    
    def test_account(self):
        client = l.Client()
        assert client.account().get().get('affiliation') == 'Scholars Portal'
        assert client.account().get().get('owner') == 'sp-demo'

    def test_account_id(self):
        client = l.Client(filepath={'config': config_test_file, 
                                    'credentials': credentials_test_file})
        assert client.account_id() == 1513

    def test_chats_list_month(self):
        client = l.Client(filepath={'config': config_test_file, 
                                    'credentials': credentials_test_file})
        chats = client.chats()
        #<class 'lh3.api._Chats'>
        chats_this_month = chats.list_month(2019,5)
        assert len(chats_this_month) >= 20
  
    def test_chats_list_day(self):
        client = l.Client(filepath={'config': config_test_file, 
                                    'credentials': credentials_test_file})
        chats = client.chats()
        #<class 'lh3.api._Chats'>
        chats_this_day = chats.list_day(2019,5, 23)
        assert len(chats_this_day) == 75

    def test_chats_list_year(self):
        client = l.Client(filepath={'config': config_test_file, 
                                    'credentials': credentials_test_file})
        chats = client.chats()
        #<class 'lh3.api._Chats'>
        chats_this_year = chats.list_year(2019)
        assert (len(chats_this_year) <= 365) and (len(chats_this_year) >= 50)

    def test_find_queue_by_name(self):
        client = l.Client(filepath={'config': config_test_file, 
                                    'credentials': credentials_test_file})
        client.set_options(version = 'v1')
        queue = client.find_queue_by_name('toronto')
        assert queue.get().get('id') == 8544
        assert queue.get().get('type') == 'queue'
    
    def test_find_user_by_name(self):
        client = l.Client(filepath={'config': config_test_file, 
                                    'credentials': credentials_test_file})
        client.set_options(version = 'v1')
        user = client.find_user_by_name('guinsly_sp')
        assert user.get().get('id') == 33045
        assert user.get().get('type') == 'user'

    def test_check_presence_sp(self):
        res_time = isNowInTimePeriod(time(10,00), time(17,00), time())
        today = datetime.datetime.today()
        if today.isoweekday():
            if res_time:
                url = 'https://ca.libraryh3lp.com/presence/jid/scholars-portal/chat.ca.libraryh3lp.com/text'
                result = requests.get(url).content.decode('UTF-8')
                assert result == 'available'
        else:
            pass

    def test_check_presence_clavardez(self):
        res_time = isNowInTimePeriod(time(10,00), time(17,00), time())
        today = datetime.datetime.today()
        if today.isoweekday():
            if res_time:
                url = 'https://ca.libraryh3lp.com/presence/jid/clavardez/chat.ca.libraryh3lp.com/text'
                result = requests.get(url).content.decode('UTF-8')
                assert result == 'available', 'Clavardez should be on'
        else:
            pass
        
