#!/usr/bin/env python
# -*- coding: utf-8 -*-



# Define some exceptions
class lh3Error(Exception):
    """ Generic parent exception
    """

    pass


class ParamNotPassed(lh3Error):
    """ Raised if a parameter which is required isn't passed
    """

    pass


class UnsupportedParams(lh3Error):
    """ Raised when unsupported parameters are passed
    """

    pass


class UserNotAuthorised(lh3Error):
    """ Raised when the user is not allowed to retrieve the resource
    """

    pass


class MissingCredentials(lh3Error):
    """
    Raised when an attempt is made to create a lh3 instance
    without providing both the username and the user password
    """

    pass


class ResourceNotFound(lh3Error):
    """ Raised when a resource (item, collection etc.) could not be found
    """

    pass


class HTTPError(lh3Error):
    """ Raised for miscellaneous URLLib errors
    """

    pass


class AuthenticationError(lh3Error):
    """ Raised for miscellaneous URLLib errors
    """

    pass

class CouldNotReachURL(lh3Error):
    """ Raised when we can't reach a URL
    """

    pass


class TooManyRequests(lh3Error):
    """
    429 - Raised when Too many unfinished uploads.
    Try again after the number of seconds specified in the Retry-After header.
    """

    pass


class ConfigFileDoesNotExist(lh3Error):
    """
    Raised when a file path to be attached can't be opened (or doesn't exist)
    """

    pass

class FileDoesNotExist(lh3Error):
    """
    Raised when a file path to be attached can't be opened (or doesn't exist)
    """

    pass

class TooManyRetries(lh3Error):
    """
    Raise after the backoff period for new requests exceeds 32s
    """
    pass
