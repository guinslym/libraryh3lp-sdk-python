.PHONY: dist publish

dist:
	python setup.py sdist
	python setup.py bdist_wheel

publish:
	twine upload dist/*

coverage:
    pytest --cov lh3

test:
	py.test --verbose tests/test_lh3.py::TestLH3API